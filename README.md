# Projet professionnel DevOps M2 2023

Bonjour,
Bienvenue sur le projet de Pierre et Mathieu.

Le but de ce projet est de mettre en place une solution de cybersécurité utilisant Wazuh, Cortex et The Hive avec Kubernetes, Helm pour utiliser ces outils pour détecter et corriger les failles de sécurité.

Ce projet est composé de deux repositories.
Le premier permet concerne le service Wazuh et permet le déploiment de The Hive, Cortex avec la base de données ElasticSearch.